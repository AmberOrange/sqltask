## SQL Task
This application gets data from a SQLite database ([Chinook database](https://github.com/lerocha/chinook-database)) and presents it in the console.
The project uses Java14, Gradle and JUnit5.