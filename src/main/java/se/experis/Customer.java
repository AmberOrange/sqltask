package se.experis;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Customer implements Fetchable {
    private final int customerId;
    private String firstName;
    private String lastName;

    public Customer(int customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getStatement() {
        return "SELECT FirstName, LastName FROM Customer WHERE CustomerId="+customerId;
    }

    public boolean fetchData(ResultSet resultSet) {
        try {
            if(resultSet.next()) {
                firstName = resultSet.getString("FirstName");
                lastName = resultSet.getString("LastName");
                return true;
            } else {
                return false;
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return false;
        }
    }
}
