package se.experis;

import java.sql.ResultSet;

public interface Fetchable {
    // Gives back a statement string the database uses
    // to retrieve specific data for the interface implementation
    String getStatement();
    // Gets a result set from the database and stores
    // the information it needs as instance variables
    boolean fetchData(ResultSet resultSet);
}
