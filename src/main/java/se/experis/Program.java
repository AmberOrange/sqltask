package se.experis;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Database database;
        int customerCount;

        // Connect to the database and check if it was successful
        System.out.println("Establishing connection to the database...");
        database = new Database("resource:Chinook_Sqlite.sqlite");
        if(!database.isConnected()) {
            System.out.println("Failed to connect to the database. Terminating program.");
            return;
        }
        System.out.println("Success!");

        // Get the amount of customers in the customer table
        ResultSet resultSet = database.getResultSetFromQuery("SELECT COUNT(*) FROM Customer");
        try {
            resultSet.next();
            customerCount = resultSet.getInt("COUNT(*)");
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            database.closeConnection();
            return;
        }

        // Prepare the user input
        int choice = 0;
        Scanner scanner = new Scanner(System.in);

        // Only accept an integer between 1 and the amount of customers
        while (choice < 1 || choice > customerCount) {
            System.out.printf("Please enter a number between 1 and %d (Leave blank for random)%n>", customerCount);
            String choiceString = scanner.nextLine();
            if(choiceString.isEmpty()) {
                choice = (int)(Math.random() * (customerCount-1)+1);
                System.out.printf("Picked customer %d%n", choice);
            }
            try {
                choice = Integer.parseInt(choiceString);
            } catch (NumberFormatException ignored) {
            }
        }

        // Fill customer with data from the database and present
        Customer customer = new Customer(choice);
        database.fetchDataIntoObject(customer);
        System.out.printf("Customer name: %s %s%n", customer.getFirstName(), customer.getLastName());

        // By using the "CustomerGenreCollection" class, we can get a list of
        // a user's favourite genres to present
        CustomerGenreCollection customerGenreCollection = new CustomerGenreCollection(choice);
        database.fetchDataIntoObject(customerGenreCollection);
        ArrayList<String> genreList = customerGenreCollection.getMostPopularGenres();
        if(genreList.size() == 0) {
            System.out.println("Could not find any preferences");
        } else if(genreList.size() == 1) {
            System.out.printf("Favourite genre: %s%n", genreList.get(0));
        } else {
            System.out.printf("Favourite genres:%n");
            for(String genre : genreList) {
                System.out.print(genre);
            }
        }

        // Finally, close connection
        database.closeConnection();
    }
}
