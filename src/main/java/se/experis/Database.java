package se.experis;

import java.sql.*;

public class Database {
    private Connection conn;

    public Database(String databaseLocation) {
        openConnection(databaseLocation);
    }

    public Database() {

    }

    // Opens a connection to the desired database
    public boolean openConnection(String databaseLocation) {
        StringBuilder stringBuilder = new StringBuilder(databaseLocation);
        stringBuilder.insert(0, "jdbc:sqlite::");

        try {
            conn = DriverManager.getConnection(stringBuilder.toString());

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return false;
        }
        return true;
    }

    // Check if the database connection is open at the moment
    public boolean isConnected() {
        // conn.isClosed() throws an exception that can't be ignored.
        // Encapsulate the method call with try-catch
        try {
            return conn != null && !conn.isClosed();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return false;
        }
    }

    // Close the connection to the database
    public boolean closeConnection() {
        if(!isConnected()) {
            return false;
        }
        try {
            conn.close();
            return true;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return false;
        }
    }

    // Calls "getStatement" from the Fetchable interface to
    // get te specific statement to fill the object instance
    // Then calls "fetchData" to have the object itself get
    // the specific data from the result set it needs
    public boolean fetchDataIntoObject(Fetchable fetchable) {
        try {
            String statement = fetchable.getStatement();
            PreparedStatement preparedStatement = conn.prepareStatement(statement);
            ResultSet resultSet = preparedStatement.executeQuery();
            return fetchable.fetchData(resultSet);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return false;
        }
    }

    // A function to call if making a Fetchable
    public ResultSet getResultSetFromQuery(String statement) {
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(statement);
            return preparedStatement.executeQuery();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return null;
        }
    }
}
