package se.experis;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class CustomerGenreCollection implements Fetchable {
    private final int customerId;
    private final ArrayList<String> genreList;

    public CustomerGenreCollection(int customerId) {
        this.customerId = customerId;
        genreList = new ArrayList<>();
    }


    public ArrayList<String> getMostPopularGenres() {
        // To get all unique strings from the list; use a set
        Set<String> genreSet = new HashSet<>(genreList);
        ArrayList<String> results = new ArrayList<>();
        int occurrence = 0;
        // Loop all unique strings
        for (String genreName : genreSet) {
            // Check the frequency of the string in the list
            int currentOccurrence = Collections.frequency(genreList, genreName);
            // If we've encountered a string with a higher frequency,
            // clear out the previous results
            if (occurrence < currentOccurrence) {
                occurrence = currentOccurrence;
                results.clear();
                results.add(genreName);
            } else if (occurrence == currentOccurrence) {
                // ... Otherwise if it's the same frequency as the
                // current result: Add it
                results.add(genreName);
            }
        }
        return results;
    }

    public String getStatement() {
        return "SELECT genre.Name FROM customer, invoice, invoiceline, track, genre " +
                "WHERE customer.customerId=invoice.customerId " +
                "AND invoice.InvoiceId=invoiceline.InvoiceId " +
                "AND invoiceline.trackId=track.trackId " +
                "AND track.genreId=genre.genreId " +
                "AND customer.CustomerId=" + customerId;
    }

    public boolean fetchData(ResultSet resultSet) {
        try {
            while(resultSet.next()) {
                genreList.add(resultSet.getString("Name"));
            }
            return true;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            return false;
        }
    }
}
