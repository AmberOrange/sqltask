package se.experis;

import org.junit.jupiter.api.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

class DatabaseTest {

    @Test
    void nothing() {
    }

    @Test
    void establishConnection() {
        Database database = new Database("resource:Chinook_Sqlite.sqlite");
        assertTrue(database.isConnected());
        assertTrue(database.closeConnection());
    }

    @Test
    void establishWrongConnection() {
        Database database = new Database();
        assertFalse(database.openConnection("resource:MyDatabase.sqlite"));
        assertFalse(database.isConnected());
        assertFalse(database.closeConnection());
    }

    @Test
    void getCustomerDetails() {
        Database database = new Database("resource:Chinook_Sqlite.sqlite");
        Customer customer = new Customer(7);
        assertTrue(database.fetchDataIntoObject(customer));
        assertEquals("Astrid Gruber",String.format("%s %s", customer.getFirstName(), customer.getLastName()));
    }

    @Test
    void getMostPopularGenreFromCustomer() {
        Database database = new Database("resource:Chinook_Sqlite.sqlite");
        CustomerGenreCollection customerGenreCollection = new CustomerGenreCollection(56);
        assertTrue(database.fetchDataIntoObject(customerGenreCollection));

        ArrayList<String> popularGenres = customerGenreCollection.getMostPopularGenres();
        System.out.println("Most popular genres:");
        for(String genre : popularGenres) {
            System.out.printf("%s%n", genre);
        }
        assertIterableEquals(asList("Rock", "Alternative & Punk"), popularGenres);
    }

    @Test
    void getNumberOfCustomerRows() {
        Database database = new Database("resource:Chinook_Sqlite.sqlite");
        ResultSet resultSet = database.getResultSetFromQuery("SELECT COUNT(*) FROM Customer");
        assertNotNull(resultSet);

        try {
            resultSet.next();
            int rowCount = resultSet.getInt("COUNT(*)");
            assertEquals(59, rowCount);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            fail();
        }
    }
}